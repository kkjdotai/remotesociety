# RemoteSociety

Technology empowers us to create the future of our choosing. From what we do at work, where we live, how firms and markets are structured... we could leverage this power to unlock our potential as individuals, and at the same time build us all up.